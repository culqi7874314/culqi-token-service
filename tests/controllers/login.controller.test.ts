import nock from 'nock';
import { createTestAppConnection } from "../utils/connect.utils";
import App from "../../src/app";
import request from "supertest";
import { Config } from '../../src/common/environment/config.env';
import { responseCodes } from '../../src/common/interfaces/response.interface';

describe('Test login controller', () => {
    let app: App;
    const baseUrl = '/api/login';

    beforeAll(async () => {
        app = await createTestAppConnection();
    });

    beforeEach(async () => {
        nock.cleanAll();
    });

    afterAll(async () => {
        nock.cleanAll();
    });

    it('Should successfully login and get commerceId', async function () {
        const result = await request(app.getServer())
            .post(baseUrl)
            .send({
                username: Config.commerceTest.username,
                password: Config.commerceTest.password
            })
            .expect(responseCodes.OK);
        expect(result.body).toMatchObject({id: Config.commerceTest.id});
    });

    it('Should not login - invalid credentials', async function () {
        const result = await request(app.getServer())
            .post(baseUrl)
            .send({
                username: 'test',
                password: 'password'
            })
            .expect(responseCodes.UNAUTHORIZED);
        expect(result.body).toBe("Usuario y/o contraseña inválida")
    });

})
