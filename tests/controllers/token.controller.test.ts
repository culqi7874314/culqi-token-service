import nock from 'nock';
import { createTestAppConnection } from "../utils/connect.utils";
import App from "../../src/app";
import request from "supertest";
import { invalidCardBecauseLuhn, tokenMock, validAmexCard } from '../utils/mock.utils';
import { Config } from '../../src/common/environment/config.env';
import * as dynamoose from 'dynamoose'
import { generate16AlmostUniqueId } from '../../src/common/utils/helpers';
import { TokenInfoModel } from '../../src/domain/model/token.model';
import { validationErrors } from '../../src/common/interfaces/messages.interface';
import { responseCodes } from '../../src/common/interfaces/response.interface';

const tokenIdMock = generate16AlmostUniqueId();

jest.mock("dynamoose", () => ({
    ...jest.requireActual("dynamoose"),
    model: () => {
        return {
            create: () => {
                return { id:tokenIdMock, ...tokenMock }
            },
            get : ({ id } : { id:string} ) => {
                if(id === tokenIdMock ) return tokenMock
                return null
            }
        }
    },
}));

jest.spyOn(dynamoose.aws.ddb.DynamoDB.prototype, 'constructor' as any).mockImplementation(jest.fn())
jest.spyOn(dynamoose.aws.ddb, 'set' as any).mockImplementation(jest.fn())

describe('Test token controller', () => {
    let app: App;
    const baseUrl = '/api/token';

    beforeAll(async () => {
        app = await createTestAppConnection();
    });

    beforeEach(async () => {
        nock.cleanAll();
    });

    afterAll(async () => {
        nock.cleanAll();
    });

    it('Should generate a token by given card fields', async function () {
        const result = await request(app.getServer())
            .post(baseUrl)
            .set('Authorization',`Bearer ${Config.commerceTest.id}`)
            .send(tokenMock)
            .expect(responseCodes.OK);
        expect(result.body).toBeDefined();
        expect(result.body.token).toBe(tokenIdMock);
        expect(result.body.token).toHaveLength(16)
    });

    it('Should not generate a token by given card fields - authorization not send', async function () {
        const result = await request(app.getServer())
            .post(baseUrl)
            .send(tokenMock)
            .expect(responseCodes.UNAUTHORIZED);
        expect(result.body).toBe('Invalid authorization')
    });

    it('Should not generate a token by given card fields - invalid authorization', async function () {
        const result = await request(app.getServer())
            .post(baseUrl)
            .set('Authorization',`Bearer random-commerce-id`)
            .send(tokenMock)
            .expect(responseCodes.UNAUTHORIZED);
        expect(result.body).toBe('Invalid authorization')
    });

    it('Should not generate a token by given card fields - invalid commerce id', async function () {
        const result = await request(app.getServer())
            .post(baseUrl)
            .set('Authorization',`Bearer invalid-24dig-commerceId`)
            .send(tokenMock)
            .expect(responseCodes.UNAUTHORIZED);
        expect(result.body).toBe('Invalid commerce id')
    });

    it('Should not generate a token by given card fields - missing key', async function () {
        const tokenMockMissingField = JSON.parse(JSON.stringify(tokenMock))
        delete tokenMockMissingField.email
        const result = await request(app.getServer())
            .post(baseUrl)
            .set('Authorization',`Bearer ${Config.commerceTest.id}`)
            .send(tokenMockMissingField)
            .expect(responseCodes.BAD_REQUEST);
        expect(result.body).toBe(validationErrors.keysRequired(['email']))
    });

    it('Should not generate a token by given card fields - key not string', async function () {
        const tokenMockInvalidKey = {...tokenMock, cvv: true}
        const result = await request(app.getServer())
            .post(baseUrl)
            .set('Authorization',`Bearer ${Config.commerceTest.id}`)
            .send(tokenMockInvalidKey)
            .expect(responseCodes.BAD_REQUEST);
        expect(result.body).toBe(validationErrors.onlyStringAllowed)
    });

    it('Should not generate a token by given card fields - invalid card number - not numeric', async function () {
        const result = await request(app.getServer())
            .post(baseUrl)
            .set('Authorization',`Bearer ${Config.commerceTest.id}`)
            .send({...tokenMock, card_number: '432143214321TEST'})
            .expect(responseCodes.BAD_REQUEST);
        expect(result.body).toBe(validationErrors.invalidCard)
    });

    it('Should not generate a token by given card fields - invalid card number - invalid length', async function () {
        const result = await request(app.getServer())
            .post(baseUrl)
            .set('Authorization',`Bearer ${Config.commerceTest.id}`)
            .send({...tokenMock, card_number: '123456789'})
            .expect(responseCodes.BAD_REQUEST);
        expect(result.body).toBe(validationErrors.invalidCard)
    });

    it('Should not generate a token by given card fields - invalid card number - LUHN algorithm not complain', async function () {
        const result = await request(app.getServer())
            .post(baseUrl)
            .set('Authorization',`Bearer ${Config.commerceTest.id}`)
            .send({...tokenMock, card_number: invalidCardBecauseLuhn})
            .expect(responseCodes.BAD_REQUEST);
        expect(result.body).toBe(validationErrors.invalidCard)
    });

    it('Should not generate a token by given card fields - invalid cvv - not numeric', async function () {
        const result = await request(app.getServer())
            .post(baseUrl)
            .set('Authorization',`Bearer ${Config.commerceTest.id}`)
            .send({...tokenMock, cvv: 'cvv'})
            .expect(responseCodes.BAD_REQUEST);
        expect(result.body).toBe(validationErrors.invalidCvv)
    });

    it('Should not generate a token by given card fields - invalid cvv - invalid length', async function () {
        const result = await request(app.getServer())
            .post(baseUrl)
            .set('Authorization',`Bearer ${Config.commerceTest.id}`)
            .send({...tokenMock, cvv: '12345'})
            .expect(responseCodes.BAD_REQUEST);
        expect(result.body).toBe(validationErrors.invalidCvv)
    });

    it('Should not generate a token by given card fields - invalid cvv - invalid length for amex', async function () {
        const result = await request(app.getServer())
            .post(baseUrl)
            .set('Authorization',`Bearer ${Config.commerceTest.id}`)
            .send({...tokenMock, card_number: validAmexCard, cvv: '123'})
            .expect(responseCodes.BAD_REQUEST);
        expect(result.body).toBe(validationErrors.invalidCvv)
    });

    it('Should not generate a token by given card fields - invalid expiration month', async function () {
        const result = await request(app.getServer())
            .post(baseUrl)
            .set('Authorization',`Bearer ${Config.commerceTest.id}`)
            .send({...tokenMock, expiration_month: '15'})
            .expect(responseCodes.BAD_REQUEST);
        expect(result.body).toBe(validationErrors.invalidExpiration)
    });

    it('Should not generate a token by given card fields - invalid expiration year', async function () {
        const result = await request(app.getServer())
            .post(baseUrl)
            .set('Authorization',`Bearer ${Config.commerceTest.id}`)
            .send({...tokenMock, expiration_year: '1999'})
            .expect(responseCodes.BAD_REQUEST);
        expect(result.body).toBe(validationErrors.invalidExpiration)
    });

    it('Should not generate a token by given card fields - invalid email format', async function () {
        const result = await request(app.getServer())
            .post(baseUrl)
            .set('Authorization',`Bearer ${Config.commerceTest.id}`)
            .send({...tokenMock, email: 'no-email-string'})
            .expect(responseCodes.BAD_REQUEST);
        expect(result.body).toBe(validationErrors.invalidEmail)
    });

    it('Should not generate a token by given card fields - invalid email domain', async function () {
        const result = await request(app.getServer())
            .post(baseUrl)
            .set('Authorization',`Bearer ${Config.commerceTest.id}`)
            .send({...tokenMock, email: 'jhon.doe@node.js'})
            .expect(responseCodes.BAD_REQUEST);
        expect(result.body).toBe(validationErrors.invalidEmail)
    });

    it('Should get card details by given valid token', async function () {
        const result = await request(app.getServer())
            .get(`${baseUrl}/${tokenIdMock}`)
            .set('Authorization',`Bearer ${Config.commerceTest.id}`)
            .expect(responseCodes.OK);
        expect(result.body).toBeDefined();
        const returnedCardDetails: TokenInfoModel = {
            card_number: result.body.card_number,
            expiration_month: result.body.expiration_month,
            expiration_year: result.body.expiration_year,
        }
        expect(result.body).toMatchObject(returnedCardDetails)
    });

    it('Should not get card details by given token - invalid format', async function () {
        const result = await request(app.getServer())
                .get(`${baseUrl}/invalid-token`)
                .set('Authorization',`Bearer ${Config.commerceTest.id}`)
                .expect(responseCodes.BAD_REQUEST);
        expect(result.body).toBe(validationErrors.invalidToken)
    });

    it('Should not get card details by given token - not found token', async function () {
        const result = await request(app.getServer())
                .get(`${baseUrl}/1234567890abcDEF`)
                .set('Authorization',`Bearer ${Config.commerceTest.id}`)
                .expect(responseCodes.BAD_REQUEST);
        expect(result.body).toBe(validationErrors.unfoundToken)
    });

})
