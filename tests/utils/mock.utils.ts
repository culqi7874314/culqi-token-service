import { Config } from "../../src/common/environment/config.env"
import { TokenModel } from "../../src/domain/model/token.model"


export const validVisaCard = '4570348484387100';
export const invalidCardBecauseLuhn = '4111222233331112';
export const validAmexCard = '377892827316287';

export const tokenMock: TokenModel = {
    card_number: validVisaCard,
    cvv: "123",
    expiration_month: "12",
    expiration_year: "2023",
    email: "jhon.doe@gmail.com",
    commerce_id: Config.commerceTest.id
}