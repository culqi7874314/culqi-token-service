import App from '../../src/app';

let app: App;

export const createTestAppConnection = async (): Promise<App> => {
    app = new App();
    await app.start(0);
    return app;
};

