import { responseCodes, responseObject } from "../../common/interfaces/response.interface";
import { Config } from "../../common/environment/config.env";
import { CommerceCredentials } from "../model/login.model";


export const LoginService = {
    login : async ({ username, password} : CommerceCredentials) : Promise<responseObject> => {
        if(username != Config.commerceTest.username || password != Config.commerceTest.password){
            return { 
                code: responseCodes.UNAUTHORIZED, 
                message: "Usuario y/o contraseña inválida"
            }
        }
        return {
            code: responseCodes.OK,
            data: {
                id: Config.commerceTest.id
            }
        }
    },
};