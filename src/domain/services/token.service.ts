import { validationErrors } from "../../common/interfaces/messages.interface";
import { responseCodes, responseObject } from "../../common/interfaces/response.interface";
import { isValidCardLength, isValidEmail, isValidYearExpiration, validateLuhn } from "../../common/utils/helpers";
import TokenRepository from "../../infrastructure/repositories/token.repository";
import { TokenDto, TokenInfoModel, TokenModel } from "../model/token.model";
import { Config } from "../../common/environment/config.env";

const tokenFields = Object.keys(TokenDto);

export const TokenService = {


    generateToken : async (tokenBody: TokenModel) : Promise<responseObject> => {
        const receivedKeys = Object.keys(tokenBody);
        const missingKeys = tokenFields.filter(key => !receivedKeys.includes(key));
        if(missingKeys.length){
            return { 
                code: responseCodes.BAD_REQUEST, 
                message: validationErrors.keysRequired(missingKeys)
            }
        }
        if(Object.values(tokenBody).some(value => typeof value !== 'string')){
            return { 
                code: responseCodes.BAD_REQUEST, 
                message: validationErrors.onlyStringAllowed
            }
        }
        if(!/\d/.test(tokenBody.card_number) || !isValidCardLength(tokenBody.card_number) || !validateLuhn(tokenBody.card_number)){
            return { 
                code: responseCodes.BAD_REQUEST, 
                message: validationErrors.invalidCard
            }
        }
        const isAmex = tokenBody.card_number.charAt(0) === Config.cardsSetup.amex.startsWith && tokenBody.card_number.length === Config.cardsSetup.amex.length;
        if(!/\d/.test(tokenBody.cvv) || ( isAmex ? tokenBody.cvv.length!=Config.cardsSetup.amex.cvvLength : tokenBody.cvv.length != Config.cardsSetup.general.cvvLength) ) {
            return { 
                code: responseCodes.BAD_REQUEST, 
                message: validationErrors.invalidCvv
            }
        }
        if(!/^(0[1-9]|1[0-2])$/.test(tokenBody.expiration_month) || !isValidYearExpiration(tokenBody.expiration_year)) {
            return { 
                code: responseCodes.BAD_REQUEST, 
                message: validationErrors.invalidExpiration
            }
        }
        if(!isValidEmail(tokenBody.email)) {
            return { 
                code: responseCodes.BAD_REQUEST, 
                message: validationErrors.invalidEmail
            }
        }
        const tokenRegistered = await TokenRepository.registerToken(tokenBody)
        return {
            code: responseCodes.OK,
            data: {
                token: tokenRegistered.id
            }
        }
    },

    getTokenInfo : async (id: any) : Promise<responseObject> => {
        if(!/^[a-zA-Z0-9]{16}$/.test(id)){
            return { 
                code: responseCodes.BAD_REQUEST, 
                message: validationErrors.invalidToken
            }
        }
        const token = await TokenRepository.getTokenById(id);
        if(!token) {
            return {
                code: responseCodes.BAD_REQUEST,
                message: validationErrors.unfoundToken
            }
        }
        const {card_number, expiration_month, expiration_year} = token
        return {
            code: responseCodes.OK,
            data: {
                card_number,
                expiration_month,
                expiration_year
            } as TokenInfoModel
        }
    },

};