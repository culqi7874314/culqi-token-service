export interface TokenModel {
    id?: string,
    card_number: string,
    cvv: string,
    expiration_month: string,
    expiration_year: string,
    email: string,
    commerce_id: string
}

export const TokenDto: TokenModel = {
    card_number: '',
    cvv: '',
    expiration_month: '',
    expiration_year: '',
    email: '',
    commerce_id: ''
}

export type TokenInfoModel  = Pick<TokenModel, 'card_number' | 'expiration_month' | 'expiration_year' >