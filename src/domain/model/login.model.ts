export interface CommerceCredentials {
    id?: string,
    username: string,
    password: string
}