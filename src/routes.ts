import { Router } from 'express';
import { tokenController } from './application/controllers/token.controller';
import { authMiddleware } from './application/middleware/auth.middleware';
import { loginController } from './application/controllers/login.controller';

const routes: Router = Router();

routes.post('/login', loginController.login);
routes.post('/token', authMiddleware, tokenController.generateToken);
routes.get('/token/:id', authMiddleware, tokenController.getTokenInfo);

export default routes;
