import * as dynamoose from 'dynamoose';
import { TokenClassModel, TokenSchema } from '../database/entity/Token';
import { Model } from 'dynamoose/dist/Model'
import { TokenModel } from '../../domain/model/token.model';
import { Config } from '../../common/environment/config.env';
import { generate16AlmostUniqueId } from '../../common/utils/helpers';

export default class TokenRepository {
    
    static dbInstance: Model<TokenClassModel> = dynamoose.model<TokenClassModel>('token', TokenSchema, {
            expires: {
                ttl: Config.tokenExpirationInSeconds,
                attribute: 'ttl'
            }
        });

    static registerToken = async (tokenRequest: TokenModel) => {
        const id = generate16AlmostUniqueId();
        return await this.dbInstance.create({id, ...tokenRequest});
    };

    static getTokenById = async (id: string) => {
        return await this.dbInstance.get({ id });
    };
}


