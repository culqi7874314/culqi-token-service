import { Item } from 'dynamoose/dist/Item'
import * as dynamoose from 'dynamoose';
import { TokenModel } from '../../../domain/model/token.model';

export const TokenSchema = new dynamoose.Schema(
    {
        id: {
            type: String,
            hashKey: true,
            required: true
        },
        card_number: {
            type: String,
            required: true
        },
        cvv: {
            type: String,
            required: true
        },
        expiration_month: {
            type: String,
            required: true
        },
        expiration_year: {
            type: String,
            required: true
        },
        email: {
            type: String,
            required: true
        },
        commerce_id: {
            type: String,
            required: true
        }
    },
    {
        timestamps: {
            createdAt: 'CreateDate'
        }
    }
);

export class TokenClassModel extends Item implements TokenModel{
    id= '';
    card_number= '';
    cvv= '';
    expiration_month= '';
    expiration_year= '';
    email= '';
    commerce_id= '';
}