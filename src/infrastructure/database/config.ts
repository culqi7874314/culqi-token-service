import * as dynamoose from 'dynamoose';

export const instanceDatabase = async()=>{
    try {
        const ddb = new dynamoose.aws.ddb.DynamoDB({
            credentials: {
                accessKeyId: process.env.aws_access_key_id,
                secretAccessKey: process.env.aws_secret_access_key
            },
            region: process.env.region || "us-east-1"
        });
        await dynamoose.aws.ddb.set(ddb);
        console.log("[INFO] Db Online")
        return true
    } catch (error) {
        console.log("[ERROR] =>",error);
        return false
    }
}

