import * as dotenv from 'dotenv';
import sls from 'serverless-http'
import App from './app';
dotenv.config()
const app = new App();
const startServer = async (): Promise<void> => {
    await app.start();
};

if(process.env.NODE_ENV != 'production') startServer();

export const handler = sls(app.getServer());




