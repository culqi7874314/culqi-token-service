export const validationErrors = {
    keysRequired : (keys: string[]) => `Faltan algunos campos requeridos: ${keys.join(', ')}`,
    onlyStringAllowed:  'Todos los campos deben ser de tipo string',
    invalidCard:  'Número de tarjeta inválido',
    invalidCvv:  'Cvv inválido',
    invalidExpiration:  'Fecha de expiración inválida',
    invalidEmail:  'Email inválido',
    invalidToken:  'Token inválido',
    unfoundToken:  'Token ha expirado o es incorrecto',
}