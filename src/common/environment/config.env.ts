export const Config = {
    commerceTest: {
        username: 'admin',
        password: 'culqi23',
        id: 'pk_test_8r4oplbuj9lz7mtD'
    },
    tokenExpirationInSeconds: 15*60,
    cardsSetup: {
        general: {
            cvvLength: 3,
            length: {
                min: 13,
                max: 16
            }
        },
        amex: {
            startsWith: '3',
            length: 15,
            cvvLength: 4
        },
        expiration: {
            highestYear: (new Date()).getFullYear() + 5
        }
    },
    allowedEmailDomains: [
        'gmail.com',
        'hotmail.com',
        'yahoo.es'
    ]
}