import { Config } from "../environment/config.env";
import { v4 as uuidv4 } from 'uuid';

export const isValidCardLength = (cardNumber: string) => {
    return cardNumber.length >= Config.cardsSetup.general.length.min && cardNumber.length <= Config.cardsSetup.general.length.max;
}

export const isValidYearExpiration = (expiration_year: string) => {
    if(!/\d/.test(expiration_year)) return false
    const minYear = (new Date).getFullYear();
    const { highestYear } = Config.cardsSetup.expiration;
    return Number(expiration_year) >= minYear && Number(expiration_year) <= highestYear
}

export const isValidEmail = (email: string) => {
    if(!/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)) return false
    return Config.allowedEmailDomains.includes(email.split("@")[1])
}

export const validateLuhn = (cardNumber: string) =>{
    const digits = cardNumber.split('').map(Number);
    let sum = 0;
    let shouldDouble = false;
    for (let i = digits.length - 1; i >= 0; i--) {
        let digit = digits[i];
        if (shouldDouble) {
            digit *= 2;
            if (digit > 9) digit -= 9;
        }
        sum += digit;
        shouldDouble = !shouldDouble;
    }
    return sum % 10 === 0;
}

function randomUppercaseDigits(x: string) {
    let result = '';
    for (let i = 0; i < x.length; i++) {
      if (Math.random() < 0.5) {
        result += x[i].toUpperCase();
      } else {
        result += x[i];
      }
    }
    return result;
}

export const generate16AlmostUniqueId = ()=>{
    const trulyUniqueId = uuidv4();
    /*
        With this approach we get a low probability of duplication,
        if we generate ids every second per 15 minutes the probability is 0.015%,
        to get close to 1% almost 30 billion of ids is required in that range of time.
    */
    const id = trulyUniqueId.replaceAll("-","").slice(-16);
    // just to add more uniqueness
    return randomUppercaseDigits(id);
}