import { Request, Response, NextFunction } from 'express';
import { Config } from '../../common/environment/config.env';
import { responseCodes } from '../../common/interfaces/response.interface';

export const authMiddleware = async (req: Request, res: Response, next: NextFunction)=>{
    const { authorization } = req.headers
    const token = authorization?.split('Bearer ')[1];
    const messageError = token?.length != 24 ? 'Invalid authorization' : (token != Config.commerceTest.id ? 'Invalid commerce id': '');
    if(messageError) return res.status(responseCodes.UNAUTHORIZED).json(messageError);
    next();
}