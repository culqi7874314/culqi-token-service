
import { Request, Response } from 'express';
import { TokenService } from '../../domain/services/token.service';
import { responseCodes } from '../../common/interfaces/response.interface';

export const tokenController = {

    generateToken: async (req: Request, res: Response) => {
        try {
            const commerce_id = req.headers.authorization.split("Bearer ")[1]
            const response = await TokenService.generateToken({commerce_id, ...req.body});
            return res.status(response.code).json(response.data || response.message);
        } catch (error) {
            return res.status(responseCodes.INTERNAL_ERROR).json({error});
        }
    },

    getTokenInfo: async (req: Request, res: Response) => {
        const { id } = req.params;
        try {
            const response = await TokenService.getTokenInfo(id);
            return res.status(response.code).json(response.data || response.message);
        } catch (error) {
            return res.status(responseCodes.INTERNAL_ERROR).json({error});
        }
    }

};
