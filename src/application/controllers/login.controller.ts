
import { Request, Response } from 'express';
import { responseCodes } from '../../common/interfaces/response.interface';
import { LoginService } from '../../domain/services/login.service';

export const loginController = {

    login: async (req: Request, res: Response) => {
        try {
            const response = await LoginService.login(req.body);
            return res.status(response.code).json(response.data || response.message);
        } catch (error) {
            return res.status(responseCodes.INTERNAL_ERROR).json({error});
        }
    },

};
