## About

NodeJs App with express and typescript, architectured with DDD approach, documented with OpenApi3.0 and using DynamoDB as database

Hardcoded credentials =>
    username: admin
    password: culqi23

## Project Setup

```
npm install
```
In order to connect to database you have to create a programatic user with access to DynamoDb

and create a ```.env``` file , add it to the root and include the following variables:
```
aws_access_key_id=<your-aws-access>
aws_secret_access_key=<your-aws-secret>
```
You can also connect to DynamoDB local following [these steps](https://dynamoosejs.com/getting_started/Configure)
 

### Compile and Hot-Reload for Development with Nodemon

```
npm run  serve
```

### Lint with [ESLint](https://eslint.org/)

```
npm run  lint
```

### Unit Test with [Jest](https://jestjs.io/)
 Execute according your platform

```npm run  test-linux``` or ```npm run test-windows```

Coverage >95%


### How to deploy to AWS Lambda

Run these commands putting your aws credentials on it
```
npm i serverless -g
serverless config credentials --provider aws --key <PUBLIC_KEY> --secret <SECRET_KEY>
npm run build
```

After that you have to copy the files ```serverless.yml```,```docs.json``` and the folder ```node_modules``` inside the dist folder, later you just simply have to run ```serverless deploy``` and you will get your public url.


### Aditional features

* Continuous integration steps added
* Health route at /health
* Logger middleware for request and response
* Docs with [Swagger](https://swagger.io/) and OpenAPi available at /docs

### Live version [here](https://i7hfvdbqzb.execute-api.us-east-1.amazonaws.com/docs/)